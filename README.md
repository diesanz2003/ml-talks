# HP SCDS ML/AI talks 2022

This is the repository that contains the materials for the HP SCDS ML/AI talks 2022.

## Installation

It is recommended to create an environment for executing the notebooks and scripts of the talks. If you are using virtualenv, you must just execute

    virtualenv python3.9 hpscds_ml_talks

However, we recommend you to use `conda`

    conda create -n hpscds_ml_talks python=3.9

where `hpscds_ml_talks` can be whatever name you prefer for your environment.

Then, once the environment is activated, you can install the mandatory modules using the requirements.txt file

    pip install -r requirements.txt

## Sessions

Find here the sessions for the previous talks:

- Session 1 (employees only) - Introduction to Deep Learning: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-1), [MS Stream](https://web.microsoftstream.com/video/53b5bdf4-92e0-449e-832e-a221e6af1a76?channelId=1e813230-38ac-4e22-bf7f-39767812befc)
- Session 2 (employees only) - Demo Session: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-2), [MS Stream](https://web.microsoftstream.com/video/bae7eaac-9266-4988-bd5e-2dc2a86e9021?channelId=1e813230-38ac-4e22-bf7f-39767812befc)
- Session 3 (all) - Basic Math: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-3), [MS Stream](https://web.microsoftstream.com/video/3f8f008a-0cb2-4ac6-bc8a-cdcc88473c8a?channelId=1e813230-38ac-4e22-bf7f-39767812befc), [YouTube](https://www.youtube.com/watch?v=WBWa9HnjAJE)
